<?php

namespace Bfa;

abstract class Hooks
{
  protected $dir;

  public function __construct(array $cfg)
  {
    $this->dir = $cfg['dir'];
  }

  abstract public function beforePull();
  abstract public function afterPull();
}
