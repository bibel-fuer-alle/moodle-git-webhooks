<?php

namespace Bfa;

require_once('hooks.class.php');
// TODO: Traverse parent directories to find Moodle's config.php, rather than assuming it's 3 levels up.
require_once(__DIR__ . '/../../../../config.php');
registerErrorHandler(); // Overwrite Moodle's error handler, which is not essential. Shutting down of DB happens elsewhere.
require_once(__DIR__ . '/../sync/page-sync.class.php');
require_once(__DIR__ . '/../sync/table-sync.class.php');

class CourseContentHooks extends Hooks
{
  private $finalPushNeeded = false;
  private $syncObjectCache = [];

  public function beforePull()
  {
    $this->finalPushNeeded = false;
    $this->executeForEachSyncItem(function (string $syncType, array $syncItem) {
      $this->finalPushNeeded = $this->finalPushNeeded || $this->getSyncClass($syncType, $syncItem)->beforePull();
    });
  }

  public function afterPull()
  {
    /** @var \Bfa\Logger $logger */
    global $logger;
    $this->executeForEachSyncItem(function (string $syncType, array $syncItem) {
      $this->getSyncClass($syncType, $syncItem)->afterPull();
    });
    if ($this->finalPushNeeded) {
      $logger->log('*** pushing changes from within Moodle ***');
      executeShellCommand("git -C {$this->dir} push -q");
      $this->finalPushNeeded = false;
    }
  }

  private function executeForEachSyncItem(callable $callback)
  {
    $syncConfig = json_decode(file_get_contents("{$this->dir}sync.json"), true);
    if (!$syncConfig) {
      throw new \Exception('Unable to parse sync.json');
    }
    foreach ($syncConfig as $syncType => $syncItems) {
      // Iterate through each item in sync.json
      foreach ($syncItems as $syncItem) {
        $callback($syncType, $syncItem);
      }
    }
  }

  /** Get specific syncClass. The object is cached to ensure the same one is used for beforePull() and afterPull(). */
  private function getSyncClass(string $syncType, array $syncItem): SyncBase
  {
    $key = serialize([$syncType, $syncItem]);
    if (!array_key_exists($key, $this->syncObjectCache)) {
      switch ($syncType) {
        case 'pages':
          $this->syncObjectCache[$key] = new PageSync($syncItem, $this->dir);
          break;
        case 'tables':
          $this->syncObjectCache[$key] = new TableSync($syncItem, $this->dir);
          break;
        default:
          throw new \Exception("Unsupported sync type $syncType");
      }
    }
    return $this->syncObjectCache[$key];
  }
}
