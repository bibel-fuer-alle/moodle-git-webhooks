#!/bin/bash

# Check arguments
if [[ $# -ne 3 ]]; then
  echo "USAGE: psql-import <database> <table> <file>"
  echo "Postgres user and password need to be set via PGUSER and PGPASSWORD environment variables"
  exit 1;
fi
database="$1"
table="$2"
file="$3"

# Rough check of CSV validity: number of columns must not be larger than at header.
# Ideally one would need to replace all data between quotation marks by single word to
# do this properly. Then the number of columns would be equal in each row.
# Without this, data in quotation marks with line breaks messes up this simple check.
numberOfColumnsInHeader=$(head -1 $file | awk -F'\t' '{print NF}')
maxNumberOfColumnsInData=$(awk -F'\t' '{print NF}' $file | sort -u | tail -1)
if [ $numberOfColumnsInHeader -ne $maxNumberOfColumnsInData ]; then
  echo "One row contains more columns than the header!"
  exit 1
fi

# Get field names, setClause, and whereClause
header=$(head -1 $file)
array=(${header//\\t/ })
setClause=""
whereClause=""
fields=""
idMissing=true
for i in "${!array[@]}"
do
  field=${array[i]}
  [ "$fields" != "" ] && fields+=","
  fields+=$field
  [ "$setClause" != "" ] && setClause+=","
  if [ "$field" != "id" ]; then
    setClause+="$field = t.$field"
    [ "$whereClause" != "" ] && whereClause+=" OR "
    whereClause+="$table.$field != t.$field"
  else
    idMissing=false
  fi
done

# Validate data
if [ idMissing = true ]; then
  echo "File needs to contain header row and column 'id'"
  exit 1
fi
if [ "$setClause" = "" ]; then
  echo "No columns apart from 'id' present, so no import possible."
  exit 1
fi

# Import file
psql $database << EOF
  CREATE TEMPORARY TABLE t AS TABLE $table WITH NO DATA;
  \copy t($fields) FROM '$file' WITH CSV HEADER DELIMITER E'\t';
  UPDATE $table SET $setClause FROM t WHERE $table.id = t.id AND ($whereClause);
EOF
