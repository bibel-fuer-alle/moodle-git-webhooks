<?php

namespace Bfa;

abstract class SyncBase
{
  protected $syncItem;
  protected $dir;

  public function __construct(array $syncItem, string $dir)
  {
    $this->syncItem = $syncItem;
    $this->dir = $dir;
  }

  abstract public function beforePull(): bool;

  abstract public function afterPull();

  protected function getFilePath($basePath, $text)
  {
    $filePrefix = '/^file:\/{0,3}/i';
    $textWithoutFilePrefix = preg_replace($filePrefix, '', $text);
    if (!$textWithoutFilePrefix) {
      return false;
    }
    return $basePath . $textWithoutFilePrefix;
  }

  protected function assertProperty(string $key, string $type, bool $requiredAndNotEmpty)
  {
    $value = $this->syncItem[$key];
    if (isset($value) && gettype($value) !== $type) {
      throw new \Exception("Property '$key' must be of type '$type'.");
    }
    if ($requiredAndNotEmpty && (!isset($value) || empty($value))) {
      throw new \Exception("Property '$key' of type '$type' is required and must not be empty.");
    }
  }
}
