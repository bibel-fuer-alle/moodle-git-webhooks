<?php

namespace Bfa;

require_once('sync-base.class.php');

class PageSync extends SyncBase
{
  private $moodlePage;

  public function __construct(array $syncItem, string $dir)
  {
    global $DB;
    parent::__construct($syncItem, $dir);
    $this->assertProperty('cmid', 'string', true);
    $this->assertProperty('content', 'string', true);
    $this->assertProperty('intro', 'string', false);
    if (!$cm = get_coursemodule_from_id('page', $syncItem['cmid'])) {
      throw new \Exception("Course module for page with id {$syncItem['cmid']} not found.");
    }
    $this->moodlePage = $DB->get_record('page', ['id' => $cm->instance], '*', MUST_EXIST);
  }

  public function beforePull(): bool
  {
    /** @var \Bfa\Logger $logger */
    global $logger;
    // Iterate through all entries
    $updateNeeded = false;
    foreach ($this->syncItem as $key => $value) {
      if ($key !== 'cmid') {
        $file = $this->getFilePath($this->dir, $value);
        if ($file) {
          $fileExists = file_exists($file);
          $value = $fileExists ? file_get_contents($file) : '';
          $diff = diffStrings($value, $this->moodlePage->{$key});
          if (trim($diff)) {
            if ($fileExists) {
              $logger->log("=== '$key' of page '{$this->moodlePage->name}' ({$this->syncItem['cmid']}) has changed on Moodle: ===");
              $logger->log("$key:\n$diff");
            } else {
              $logger->log("=== '$key' of page '{$this->moodlePage->name}' ({$this->syncItem['cmid']}) has been added as new file '$file' ===");
            }
            file_put_contents($file, $this->moodlePage->{$key});
            $updateNeeded = true;
          }
        }
      }
    }
    if ($updateNeeded) {
      $logger->log('*** Committing changes from within Moodle ***');
      executeShellCommand("git -C {$this->dir} add .");
      executeShellCommand("git -C {$this->dir} commit -m 'Updates from within Moodle' -q");
      return true;
    }
    return false;
  }

  public function afterPull()
  {
    /** @var \Bfa\Logger $logger */
    global $DB, $logger;
    // TODO: 'name' not yet supported, as it isn't changed in other places where it is being used.
    $permittedKeys = ['intro', 'content'];
    // Iterate through all entries
    $updateNeeded = false;
    foreach ($this->syncItem as $key => $value) {
      if ($key !== 'cmid') {
        if (!in_array($key, $permittedKeys)) {
          $logger->log("Invalid key: $key");
          continue;
        }
        $path = $this->getFilePath($this->dir, $value);
        if ($path && file_exists($path)) {
          $value = file_get_contents($path);
        }
        $diff = diffStrings($this->moodlePage->{$key}, $value);
        if (trim($diff)) {
          if (!$updateNeeded) {
            $logger->log("=== '{$this->moodlePage->name}' ({$this->syncItem['cmid']}) will be updated: ===");
            $updateNeeded = true;
          }
          $logger->log("$key:\n$diff");
          $this->moodlePage->{$key} = $value;
        }
      }
    }
    if ($updateNeeded) {
      $this->moodlePage->revision++;
      $DB->update_record('page', $this->moodlePage);
    } else {
      $logger->log("--- '{$this->moodlePage->name}' ({$this->syncItem['cmid']}) unchanged ---");
    }
  }
}
