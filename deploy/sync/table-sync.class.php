<?php

namespace Bfa;

require_once('sync-base.class.php');

class TableSync extends SyncBase
{
  private $file = '';
  private $table = '';
  private $columns = [];
  private $condition = '';
  private $fileContentBeforePull = '';

  public function __construct(array $syncItem, string $dir)
  {
    parent::__construct($syncItem, $dir);
    $this->assertProperty('content', 'string', true);
    $this->assertProperty('table', 'string', true);
    $this->assertProperty('columns', 'array', false);
    $this->assertProperty('condition', 'string', false);
    $this->file = $this->getFilePath($dir, $syncItem['content']);
    if (!$this->file) {
      throw new \Exception("Invalid file path in property 'content'. It needs to start with 'file:'.");
    }
    $this->table = $syncItem['table'];
    $this->condition = $syncItem['condition'] ? $syncItem['condition'] : '';
    $columns = $syncItem['columns'];
    if ($columns) {
      array_unshift($columns, 'id');
      $this->columns = array_unique($columns);
    } else if ($this->condition) {
      // TODO: Add support for this
      throw new \Exception("The underlying export script only supports a condition when the columns are also specified.");
    }
  }

  public function beforePull(): bool
  {
    /** @var \Bfa\Logger $logger */
    global $logger;
    $fileExists = file_exists($this->file);
    $this->executeAction('export');
    $this->fileContentBeforePull = $fileExists ? file_get_contents($this->file) : '';
    $diff = diffFile($this->file, true);
    if (trim($diff) || !$fileExists) {
      if ($fileExists) {
        $logger->log("=== Table '{$this->table}' has changed in Moodle database: ===");
        $logger->log("$diff");
      } else {
        $logger->log("=== Table '{$this->table}' has been added as new file '{$this->file}' ===");
      }
      $logger->log('*** Committing changes from within Moodle database ***');
      executeShellCommand("git -C {$this->dir} add {$this->file}");
      executeShellCommand("git -C {$this->dir} commit -m 'Updates from within Moodle database' -q");
      return true;
    }
    return false;
  }

  public function afterPull()
  {
    /** @var \Bfa\Logger $logger */
    global $logger;
    if (file_exists($this->file)) {
      $fileContentAfterPull = file_get_contents($this->file);
      $diff = diffStrings($this->fileContentBeforePull, $fileContentAfterPull, true);
      if (trim($diff)) {
        $logger->log("=== Table '{$this->table}' will be updated: ===");
        $logger->log("$diff");
        $this->executeAction('import');
        $this->clearCache();
      } else {
        $logger->log("--- Table '{$this->table}' unchanged ---");
      }
    }
  }

  private function executeAction(string $action)
  {
    global $CFG;
    $args = "\"{$CFG->dbname}\" \"{$this->table}\" \"{$this->file}\"";
    if ($action === 'export') {
      if ($this->columns) {
        $args .= ' "' . implode(',', $this->columns) . '"';
        if ($this->condition) {
          $escapedCondition = preg_replace('/"/', "'", $this->condition);
          $args .= " \"{$escapedCondition}\"";
        }
      }
    }
    $cmd = <<<EOT
  export PGUSER="{$CFG->dbuser}";
  export PGPASSWORD="{$CFG->dbpass}";
  ./psql-$action.sh 2>&1 $args;
EOT;
    $output = [];
    exec($cmd, $output, $returnCode);
    $output = join("\n", $output);
    echo $output;
    if ($returnCode != 0 || preg_match('/ERROR:/', $output)) {
      throw new \ErrorException("Failed to execute psql $action:\n" . $output);
    }
  }

  private function clearCache()
  {
    /** @var \Bfa\Logger $logger */
    global $CFG, $logger;
    $cache = '';
    // The following regex may include tables that do not require a clearing of the questiondata cache,
    // but it's safest and easiest to include all here.
    if (preg_match('/' . $CFG->prefix . '(question|qtype)[_$]/', $this->table)) {
      $cache = 'core_questiondata';
    }
    if ($cache) {
      $path = "{$CFG->dataroot}/cache/cachestore_file/default_application/$cache";
      try {
        if (file_exists($path)) {
          executeShellCommand("rm -rf $path");
          $logger->log("--- Cache '$cache' cleared ---");
        }
      } catch (\Exception $e) {
        $logger->log("--- Unable to clear '$cache' cache: ---");
        $logger->log($e->getMessage());
      }
    }
  }
}
