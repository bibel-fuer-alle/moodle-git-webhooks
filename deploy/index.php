<?php

namespace Bfa;

/**
 * @var bool        $executionSuccessful
 * @var bool        $forbidCalled
 */
$executionSuccessful = false;
$forbidCalled = false;

try {
  require_once('../helpers/error-handlers.php');
  require_once('../helpers/deploy-helpers.php');
  require_once('../helpers/http-helpers.php');
  require_once('../helpers/logger.class.php');
  $logger = new Logger('deploy');

  require_once('hooks/course-content-hooks.class.php');

  if (file_exists('config.php')) {
    require_once('config.php');
  } else {
    throw new \Exception('Please define a config.php file as described in the README.md.');
  }

  $logger->log("\n~~~~~~> Start of webhook handler ~~~~~~>");
  $logger->logTimeString();

  $content = file_get_contents('php://input');
  $json = json_decode($content, true);

  $matchingRepo = false;

  foreach ($WEBHOOKS_CONFIG as $cfg) {
    // Check for matching repository name
    if (stripos($cfg['remoteRepository'], ":{$json['repository']['full_name']}.git") !== false) {
      $matchingRepo = true;
      $logger->log('Matching repo found: ' . $cfg['remoteRepository']);

      //--- Check token
      if (!checkToken($cfg, $content)) {
        continue;
      }

      //--- Check branch name
      $branch = array_key_exists('ref', $json) ? $json['ref'] : $json['push']['changes'][0]['new']['name'];
      if ($branch !== $cfg['branch']) {
        $logger->log('=== Info: Pushed branch does not match $cfg["branch"] ===');
        continue;
      }
      $logger->log("Branch: $branch");
      try {
        $logger->log("Diff: {$json['push']['changes'][0]['links']['diff']['href']}");
      } finally {
      }

      //--- Check directory
      if (!file_exists("{$cfg['dir']}.git") || !is_dir($cfg['dir'])) {
        $logger->log('=== ERROR: $cfg["dir"] is not a repository ===');
        forbid("{$cfg["dir"]} is not a repository");
      }

      // return OK now to prevent subsequent timeouts
      ok();

      //--- Execute stash & hooks & pull
      try {
        stashChanges($cfg);
        executeHook($cfg, 'beforePull');
        executePull($cfg);
        executeHook($cfg, 'afterPull');
      } finally {
        unstashChanges($cfg);
      }
      break;
    }
  }

  if (!$matchingRepo) {
    $logger->log('No matching repo found for:');
    $logger->log($content);
    $logger->log($json['repository']['full_name']);
    $logger->log(json_encode($WEBHOOKS_CONFIG, JSON_PRETTY_PRINT));
    forbid('No matching repo found');
  }
  $executionSuccessful = true;
} catch (\Throwable $e) {
  global $logger;
  if ($logger) {
    $logger->log("=== UNCAUGHT ERROR ===");
    $logger->log($e);
  }
  forbid('Uncaught error');
  throw $e;
} finally {
  $errorOccured = $forbidCalled || !$executionSuccessful;
  if ($logger) {
    if ($errorOccured) {
      $logger->log('=== An error occurred ===');
    }
    $logger->log("<~~~~~~ End of webhook handler <~~~~~~\n");
    $logger->sendEmail($errorOccured);
  }
}
