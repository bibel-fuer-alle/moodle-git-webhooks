#!/bin/bash

# Check arguments
if [[ $# -lt 3 || $# -gt 5 ]]; then
  echo "USAGE: psql-export <database> <table> <file> [<comma-separated-column-names>] [<whereClause>]"
  echo "Postgres user and password need to be set via PGUSER and PGPASSWORD environment variables"
  exit 1;
fi
database="$1"
table="$2"
file="$3"
if [[ "$4" = "" ]]; then
  names="*"
else
  names="$4"
  idRegex=\\bid\\b
  [[ $names =~ $idRegex ]] || echo "Warning: Without the column 'id', re-import will be impossible."
fi
if [[ "$5" != "" ]]; then
  whereClause="WHERE $5"
fi

# Execute export
psql $database << EOF
  \copy ( SELECT $names FROM $table $whereClause ORDER BY id) TO '$file' WITH CSV HEADER DELIMITER E'\t';
EOF
