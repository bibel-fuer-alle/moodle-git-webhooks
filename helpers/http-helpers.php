<?php

namespace Bfa;

/**
 * Forbid access
 */
function forbid(string $reason)
{
  /** @var \Bfa\Logger $logger */
  global $forbidCalled, $logger;
  $forbidCalled = true;
  // explain why
  if ($reason) $logger->log('=== ERROR: ' . $reason . ' ===');
  $logger->log("*** ACCESS DENIED ***\n\n");

  // forbid
  header('HTTP/1.0 403 Forbidden');
};

/**
 * Return OK
 */
function ok()
{
  ob_start();
  header('HTTP/1.1 200 OK');
  header('Connection: close');
  header('Content-Length: ' . ob_get_length());
  ob_end_flush();
  ob_flush();
  flush();
}
