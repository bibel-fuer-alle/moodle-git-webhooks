<?php

namespace Bfa;

/** @var bool $stashNeeded */
$stashNeeded = false;
$hooksCache = [];

/**
 * Ensure token is correct
 */
function checkToken(array $cfg, string $content): bool
{
  $error = '';
  $token = false;

  // retrieve the token
  if (!$token && isset($_SERVER['HTTP_X_HUB_SIGNATURE'])) {
    list($algo, $token) = explode('=', $_SERVER['HTTP_X_HUB_SIGNATURE'], 2) + ['', ''];
  } elseif (isset($_SERVER['HTTP_X_GITLAB_TOKEN'])) {
    $token = $_SERVER['HTTP_X_GITLAB_TOKEN'];
  } elseif (isset($_GET['token'])) {
    $token = $_GET['token'];
  }
  // Check for a GitHub signature
  if (!empty($cfg['token']) && isset($_SERVER['HTTP_X_HUB_SIGNATURE']) && $token !== hash_hmac($algo, $content, $cfg['token'])) {
    $error = 'X-Hub-Signature does not match $cfg["token"]';
    // Check for a GitLab token
  } elseif (!empty($cfg['token']) && isset($_SERVER['HTTP_X_GITLAB_TOKEN']) && $token !== $cfg['token']) {
    $error = 'X-GitLab-Token does not match $cfg["token"]';
    // Check for a $_GET token
  } elseif (!empty($cfg['token']) && isset($_GET['token']) && $token !== $cfg['token']) {
    $error = '$_GET["token"] does not match $cfg["token"]';
    // if none of the above match, but a token exists, exit
  } elseif (!empty($cfg['token']) && $token === false) {
    $error = 'No token detected';
  }
  if ($error) {
    forbid($error);
    return false;
  }
  return true;
};

/**
 * Execute given hook
 */
function executeHook(array $cfg, string $hookName)
{
  /** @var \Bfa\Logger $logger */
  global $logger;
  if (array_key_exists('hooks', $cfg)) {
    $hooks = getHooks($cfg);
    try {
      $logger->log("*** $hookName INITIATED ***");
      if ($hookName === 'beforePull') {
        $hooks->beforePull();
      } else {
        $hooks->afterPull();
      }
      $logger->log("*** $hookName SUCCESSFUL ***");
    } catch (\Throwable $e) {
      $logger->log("=== ERROR: $hookName FAILED ===");
      $logger->log($e);
      throw $e;
    }
  }
};

/**
 * Get (typed) hooks object from config.
 * The object is cached to use same object for each hook.
 */
function getHooks(array $cfg): Hooks
{
  global $hooksCache;
  $className = $cfg['hooks'];
  if (!array_key_exists($className, $hooksCache)) {
    $hooksCache[$className] = new $className($cfg);
  }
  return $hooksCache[$className];
}


function stashChanges(array $cfg)
{
  /** @var \Bfa\Logger $logger */
  global $stashNeeded, $logger;
  $stashNeeded = executeShellCommand("git -C '{$cfg['dir']}' diff --ignore-submodules", true)[0];
  if ($stashNeeded) {
    $logger->log('*** stashing ***');
    executeShellCommand("git -C '{$cfg['dir']}' stash -q", true);
  }
}

function unstashChanges(array $cfg)
{
  /** @var \Bfa\Logger $logger */
  global $stashNeeded, $logger;
  if ($stashNeeded) {
    $logger->log('*** unstashing ***');
    executeShellCommand("git -C '{$cfg['dir']}' stash pop -q", true);
  }
}

/**
 * Execute pull
 */
function executePull(array $cfg)
{
  /** @var \Bfa\Logger $logger */
  global $logger;
  try {
    $logger->log('*** pulling ***');
    // ignoring spaces at EOL is necessary to prevent rebase failures due to Windows-Linux line-end differences
    executeShellCommand("git -C '{$cfg['dir']}' pull --rebase -Xignore-space-at-eol -q", true);
    sleep(0.5); // The following command regularly fails. Maybe too quick after pull?
    executeShellCommand("git -C '{$cfg['dir']}' submodule update --init --recursive --remote", true);
  } catch (\Throwable $e) {
    $logger->log('=== ERROR: PULL FAILED ===');
    $message = $e->getMessage();
    $logger->log($message);
    try {
      $logger->log("~~~ Output of `git status`: ~~~\n");
      $logger->log(executeShellCommand("git -C '{$cfg['dir']}' status", true)[0]);
      $logger->log("~~~ Output of `git diff`: ~~~\n");
      $logger->log(diffDirectory($cfg['dir']));
    } catch (\Throwable $e2) {
    }
    if (preg_match('/git rebase --continue/', $message)) {
      new \Exception('The pull failed because the data has changed both on the server or in Moodle and in the repository. Please resolve the rebase conflict on the server & push to the repository again to trigger another deployment.');
    } else {
      throw $e;
    }
  }
};

function diffDirectory(string $dir, $exact = false): string
{
  $args = '--word-diff' . ($exact ? ' -U0' : ' --ignore-space-at-eol');
  return executeShellCommand("git -C $dir diff $args", true)[0];
}

function diffFile(string $file, $exact = false): string
{
  $args = '--word-diff' . ($exact ? ' -U0' : ' --ignore-space-at-eol');
  $dir = dirname($file);
  return executeShellCommand("git -C $dir diff $args -- $file", true)[0];
}

function diffStrings(string $string1, string $string2, $exact = false): string
{
  $args = '--word-diff' . ($exact ? ' -U0' : ' --ignore-space-at-eol');
  try {
    // strings can be too long for bash; save as temporary files instead
    file_put_contents('string1.tmp', $string1);
    file_put_contents('string2.tmp', $string2);
    return executeShellCommand("git diff $args --no-index string1.tmp string2.tmp", true)[0];
  } finally {
    @unlink('string1.tmp');
    @unlink('string2.tmp');
  }
}

/**
 * Execute shell command returning array of stdout and stderr output.
 */
function executeShellCommand(string $cmd, bool $throwError = null): array
{
  $pipes = [];
  $process = proc_open($cmd, [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'w']], $pipes);
  if (is_resource($process)) {
    $stdout = stream_get_contents($pipes[1]);
    $stderr = stream_get_contents($pipes[2]);
    fclose($pipes[1]);
    fclose($pipes[2]);
    proc_close($process);
    if ($throwError && $stderr) {
      throw new \Exception($stderr);
    }
    return [$stdout, $stderr];
  }
  throw new \Exception("Failed to execute shell command '$cmd'");
}
