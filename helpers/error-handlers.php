<?php

namespace Bfa;

function registerErrorHandler()
{
  set_error_handler(function ($errno, $errstr, $errfile, $errline) {
    errorHandler($errno, $errstr, $errfile, $errline);
  });
}

function errorHandler($errno, $errstr, $errfile, $errline)
{
  /** @var \Bfa\Logger $logger */
  global $logger;
  // TODO: Adjust to only report proper errors. Moodle changes error_reporting to warnings when debugging setting is adjusted.
  if (!($errno & error_reporting())) return true; // Ignore all errors not reported
  if ($logger) {
    $logger->log("ERROR: $errstr ($errno in $errfile:$errline)");
  }
  throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
}

registerErrorHandler();

register_shutdown_function(function () {
  $error = error_get_last();
  if ($error) {
    errorHandler($error['type'], $error['message'], $error['file'], $error['line']);
  }
});
