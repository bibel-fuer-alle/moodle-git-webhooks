<?php

namespace Bfa;

class Logger
{
  private $file;
  private $newLogContent = '';
  private $name;

  function __construct($name)
  {
    $this->name = $name;
    $this->file = fopen("$name.log", 'a');
  }

  function __destruct()
  {
    fclose($this->file);
  }

  /**
   * Log $text to file & store in newLogContent for email
   */
  public function log($text)
  {
    fwrite($this->file, $text . PHP_EOL);
    $this->newLogContent = $this->newLogContent . $text . PHP_EOL;
  }

  public function logTimeString()
  {
    $this->log($this->getTimeString());
  }

  public function sendEmail($errorOccured)
  {
    global $WEBHOOKS_LOG_EMAIL;
    if (isset($WEBHOOKS_LOG_EMAIL)) {
      mail($WEBHOOKS_LOG_EMAIL, "{$this->name} webhook" . ($errorOccured ? ' ERROR' : ' log') . ' - ' . $this->getTimeString(), $this->newLogContent);
    } else {
      $this->log('--- Please define $WEBHOOKS_LOG_EMAIL in config.php if you would like to receive the log file via email. ---');
    }
  }

  private function getTimeString(): string
  {
    date_default_timezone_set('UTC');
    return date('d-m-Y (H:i:s)', time());
  }
}
