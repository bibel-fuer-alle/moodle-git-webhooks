# External Version Control for Moodle

This code adds a webhooks handler that updates existing Moodle content, in particular pages, based on the content of a git repository. It is work-in-progress and might be converted to a proper Moodle plugin in the future.

## Setup

### 1. Clone this repo

Clone this repository into a new folder `git/webhooks` in your moodle web folder. (The code currently assumes its 2 levels down from the web folder root. The folder names are arbitrary.)

### 2. Add your webhooks configuration

Add a `config.php` into this folder to specify which repository you would want to sync to Moodle. You can also add other cloned repos on the server to be updated when a webhook for this repo is triggered:

```php
<?php

namespace Bfa;

$WEBHOOKS_LOG_EMAIL = '<optional email-address for logs>';

$WEBHOOKS_CONFIG = [
  [
    // A repo that is pulled and synced with Moodle (via the CourseContentHooks class)
    'token' => '<webhook token>',
    'remoteRepository' => '<repo git url>',
    'dir' => '<absolute path to repo on server>',
    'branch' => '<branch name>',
    'hooks' => 'Bfa\CourseContentHooks'
  ],
  [
    // A repo that is just pulled
    'token' => '<webhook token>',
    'remoteRepository' => '<repo git url>',
    'dir' => '<absolute path to repo on server>',
    'branch' => 'master',
  ]
];
```

### 3. Activate webhooks

Add webhooks to your git provider for each of these repos pointing to the `index.php` in `https://your-moodle.com/git/webhooks/deploy/`.

The token can be added directly to the URL, if your git hosting provider does not allow to specify it directly: `https://your-moodle.com/git/webhooks/deploy/?token=<your token>`.

The webhooks have been tested for Bitbucket, but GitHub and GitLab may also already work out-of-the-box.

### 4. Add sync definitions to your repo

Add a `sync.json` file into the repository you want to sync with Moodle.

At the moment, 2 types of syncs are supported: syncing of pages and syncing of Moodle Postgres database tables (to be used with caution!):

```js
{
  "pages": [
    {
      "cmid": "<page id>",
      "content": "<content or path to file>" // e.g. "file:page1.html"
    }
  ],
  "tables": [
    {
      "table": "<table name>",     // e.g. "mdl_question"
      "content": "<path to file>", // e.g. "file:tables/mdl_question"; it's best to have the
      "columns": [
        "<column 1>", // e.g. "name"
        "<column 2>", // e.g. "questiontext"
        "<etc.>"
      ],
      "condition": "<optional condition>" // e.g. "questiontext != '0'", which excludes all categories
    }
  ]
}
```

## The sync process

The sync triggered by a webhook call consists of the following steps:

1. If the given file does not already exist, it is created from Moodle.
2. If the data has changed on Moodle, the file is updated and a commit added for it.
3. The new commit(s) are pulled in.
4. All changes are synced to Moodle.
5. If any new commits have been created in step 1 or 2, these are now pushed to the repo.

## Integrating changes on Moodle with changes in the repo

In general, it's best to only change the content in the repository and always sync it into Moodle. But when content is changed both on Moodle and in the repository, git is usually very good at integrating these two during step 3 of the sync.

If this cannot be done automatically, because the same lines or neighbouring lines have been changed both on Moodle and in the repository, git will highlight a conflict during this pull (with rebase). An error will be logged and the conflict needs to be resolved manually and the new commit pushed to trigger the webhook again for the resolved content.

## Page & table updates

The updating of pages uses the regular Moodle API and is well tested.

The direct updating of table content, currently only implemented for PostgreSQL, is much more invasive and potentially risky. It is highly flexible, as any content stored anywhere in the Moodle database can be updated with it, but it bypasses side-effects such as cache invalidation or the updating of columns such as timemodified or modifiedby.

The table sync was added for syncing question content, but this should be replaced by using the regular Moodle API for this in the future. Any sync for one of the question tables will currently invalidate the whole _core_questiondata_ cache.

## License

MIT License, Copyright (c) 2019 Bibel für alle (http://bibel-für-alle.net)

The php GitHub/GitLab/Bitbucket-webhook handling code was taken from https://github.com/vicenteguerra/git-deploy.
